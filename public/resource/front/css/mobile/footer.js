defineClass('init', basejs, {
    init: function () {
        var it = this;
        it.onShopItemNum();
        //it.flag_status();  //屏蔽新人送5云币
        it.flag_show();
        //it.flag_activity_recharge(); 充10元送2币
        //it.onFirstLoad();
    },
    onFirstLoad: function () {
        var it = this;
        /*it.doAjax({
         url: it.WEB_PATH + '/mobile/mobile/getavatar',
         method: 'post',
         success: function (res) {
         console.log(res);
         }
         });*/
        var isFirstLoad = localStorage.getItem("isFirstLoad");
        if (isFirstLoad) {
            it.flag_status();
            it.flag_show();
            return;
        }
        localStorage.setItem("isFirstLoad", "true");
        it.onMask(['<div style="width:70%;background:#FFF;margin:125px auto;border-radius:10px;text-align:center;">',
            '<div id="isFirstLoad" class="swiper-container">',
            '<ul class = "swiper-wrapper">',
            '<li style="text-align:center;border-radius:20px;" class="swiper-slide">',
            '<a href="javascript:;"><img src="', it.G_TEMPLATES_STYLE, '/images/mobile/tan_01.png" style="width:100%;"/></a>',
            '</li>',
            '<li style="text-align:center;border-radius:20px;" class="swiper-slide">',
            '<a href="javascript:;"><img src="', it.G_TEMPLATES_STYLE, '/images/mobile/tan_02.png" style="width:100%;"/></a>',
            '</li>',
            '<li style="text-align:center;border-radius:20px;" class="swiper-slide">',
            '<a href="javascript:;"><img src="', it.G_TEMPLATES_STYLE, '/images/mobile/tan_03.png" style="width:100%;"/></a>',
            '</li>',
            '<li style="text-align:center;border-radius:20px;" class="swiper-slide">',
            '<a href="', it.WEB_PATH, '/mobile/mobile/"><img src="', it.G_TEMPLATES_STYLE, '/images/mobile/tan_04.png" style="width:100%;"/></a>',
            '</li>',
            '</ul>',
            '<div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets"></div>',
            '</div>',
            '</div>'].join(""));
        new Swiper('#isFirstLoad', {
            pagination: '#isFirstLoad .swiper-pagination',
            paginationClickable: true,
        });
    },
    onShopItemNum: function () {
        var it = this;
        var shopCarList = localStorage.getItem("shopCarList") || "[]";
        shopCarList = it.toJson(shopCarList);
        var shop_item_num = shopCarList.length;
        if (shop_item_num) {
            $(".shop_car_num").removeClass("offsethide").html(shop_item_num).show();
        } else {
            $(".shop_car_num").addClass("offsethide");
        }
    },
    flag_activity_recharge: function () {
        var it = this;
        var flag_activity_recharge = it.getCookie('flag_activity_recharge');
        if (flag_activity_recharge == 1) {
            var html = [
                '<div style="width:100%;height:100%;position:fixed;background:rgba(0,0,0,0.8);z-index:30;">',
                '<div style="position:absolute;left:50%;top:50%;width:300px;height:396px;margin-left:-150px;margin-top:-198px;">',
                '<img id="step_close_btn" style="width:30px;height:30px;position:absolute;right:0px;top:0px;cursor:pointer;padding:15px;" src="', G_TEMPLATES_STYLE, '/images/mobile/del_tw.png', '" >',
                '<img id="flag_gift" style="width:100%;cursor:pointer;" src="', G_TEMPLATES_STYLE, '/images/mobile/activ_01.png', '" >',
                '</div>',
                '</div>'
            ];
            var dom = $(html.join(""));
            $("body").prepend(dom).css('overflow', 'hidden');
            //关闭
            $("#step_close_btn", dom).off("click").on({
                click: function () {
                    it.doAjax({
                        url: WEB_PATH + '/mobile/mobile/update_activity_recharge',
                        success: function () {
                        }
                    });
                    $(this).remove();
                    dom.fadeOut(800, function () {
                        this.remove();
                    });
                }
            });
            //充值
            $("#flag_gift", dom).off("click").on({
                click: function () {
                    it.doAjax({
                        url: WEB_PATH + '/mobile/mobile/update_activity_recharge',
                        success: function (res) {
                            if (res.code != 0) {
                                it.prompt(res.msg);
                                return;
                            }
                            location.href = WEB_PATH + "/mobile/mobile/recharge";
                        }
                    });
                }
            });
        }
    },
    //后期再重构此代码成插件...
    flag_status: function () {
        var it = this;
        var flag_st_gift = it.getCookie('flag_gift');

        //判断送币状态
        if (flag_st_gift == 1) {
            it.flag_st_gift();
        }
    },
    /**
     *  活动送币
     */
    flag_st_gift: function () {
        var it = this;
        //20云币（旧版）
        /*var html = [
         '<div style="width:100%;height:100%;position:fixed;background:rgba(0,0,0,0.8);z-index:30;">',
         '<div style="position:absolute;left:50%;top:50%;width:300px;height:396px;margin-left:-150px;margin-top:-198px;">',
         '<img id="flag_gift" style="width:100%;cursor:pointer;" src="', G_TEMPLATES_STYLE, '/images/mobile/duobaobi20.png', '" >',
         '</div>',
         '</div>'
         ]*/
        var html = [
            '<div style="width:100%;height:100%;position:fixed;background:rgba(0,0,0,0.8);z-index:30;overflow:hidden;">',
            '<div id="step_close" style="z-index:31;position:absolute;left:50%;top:50%;width:300px;height:15px;margin-left:-150px;margin-top:-198px;">',
            '<img id="step_close_btn" style="width:20px;height:20px;position:absolute;right:0px;top:-4px;cursor:pointer;padding:15px;" src="', G_TEMPLATES_STYLE, '/images/mobile/lead_delete.png', '" >',
            '</div>',
            //setp 1
            '<div id="step1" class="st_panel" style="position:absolute;left:50%;top:50%;width:300px;height:396px;margin-left:-150px;margin-top:-198px;">',
            '<img id="" style="width:100%;cursor:pointer;" src="', G_TEMPLATES_STYLE, '/images/mobile/lead_01.png', '" >',
            '</div>',
            //setp 2
            '<div id="step2" class="st_panel" style="display:none;position:absolute;left:50%;top:50%;width:300px;height:396px;margin-left:-150px;margin-top:-198px;">',
            '<img id="" style="width:100%;cursor:pointer;" src="', G_TEMPLATES_STYLE, '/images/mobile/lead_02.png', '" >',
            '<div id="all_people_gift" style="z-index:31;position:absolute;top:40px;left:58px;letter-spacing:21px;font-size:25px;color:#f4483a;">123456</div>',
            '<input id="reg_phone" type="tel" placeholder="输入手机号码" validate="phone" msg="请正确输入手机号" maxlength="12" style="background:#FFF;color:#000;border-radius:25px;font-size:15px;height:39px;outline:none;border:0;width:83%;padding-left:20px;position:absolute;left:16px;right:16px;top:190px" />',
            '<input id="verifycode" type="tel" placeholder="输入验证码" validate="/^\\d{4}$/" msg="请正确输入验证码"  maxlength="6" style="background:#FFF;color:#000;border-radius:25px;font-size:15px;height:39px;outline:none;border:0;width:130px;padding-left:20px;position:absolute;left:16px;top:250px" />',
            '<button id="get_vcode" style="background:#FFF000;color:#8d3c00;border-radius:25px;font-size:14px;height:39px;outline:none;border:0;width:100px;position:absolute;right:16px;top:250px">获取验证码</button>',
            '<img id="btn_submit" src="', G_TEMPLATES_STYLE, '/images/mobile/lead_unbutton.png', '" style="height:46px;width:89%;outline:none;border:0;position:absolute;right:16px;top:340px;left:16px;" />',
            '</div>',
            //setp 3
            '<div id="step3" class="st_panel" style="display:none;position:absolute;left:50%;top:50%;width:300px;height:396px;margin-left:-150px;margin-top:-198px;">',
            '<img id="" style="width:100%;cursor:pointer;" src="', G_TEMPLATES_STYLE, '/images/mobile/lead_03.png', '" >',
            '</div>',
            '</div>'
        ];
        var dom = $(html.join(""));
        //alert(localStorage.getItem("gift_dialog"));
        if (localStorage.getItem("gift_dialog") == "true") {
            return;
        }
        $("body").prepend(dom).css('overflow', 'hidden');
        $("#step_close_btn", dom).off("click").on({
            click: function () {
                localStorage.setItem("gift_dialog", "true");
                $(this).remove();
                dom.fadeOut(800, function () {
                    this.remove();
                });
            }
        });

        //step1
        $('#step1', dom).off('click').on({
            click: function () {
                $('.st_panel', dom).hide();
                //var hsdnumber = it.getCookie('flag_hasnumber').substring(0, 3) + Math.ceil((Math.random() * 100) + 100);
                //console.log(hsdnumber);
                $('#all_people_gift', dom).html(it.getCookie('flag_hasnumber'));
                $('#step2', dom).fadeIn(500);
            }
        });

        //step2
        $('#get_vcode', dom).off('click').on({
            click: function () {
                var mobile = $('#reg_phone', dom).val();
                if (mobile.match(/(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}/) == null) {
                    it.prompt('请正确输入手机号码');
                    return;
                }
                var This = $(this);
                it.doAjax({
                    url: it.WEB_PATH + "/mobile/mobile/do_sendmsg/",
                    data: {phone: mobile},
                    method: 'post',
                    success: function (res) {
                        if (res.code != 0) {
                            it.prompt(res.msg);
                            return;
                        }
                        it.showOk({
                            text: "短信验证码已发送成功, 请注意查收!",
                            callback: function () {
                                var loop = 60;
                                var value = This.html();
                                This.css('background', '#e5e5e5').attr('disabled', 'disabled');
                                var clock = setInterval(function () {
                                    if (loop >= 0) {
                                        This.html((loop--) + ' S');
                                    } else {
                                        This.css('background', '#FFF000').removeAttr('disabled').html(value);
                                        clearInterval(clock);
                                    }
                                }, 1000);
                            }
                        });
                    }
                });
            }
        });
        $('#reg_phone,#verifycode', dom).off('keyup').on({
            keyup: function () {
                if ($('#reg_phone', dom).val() && $('#verifycode', dom).val()) {
                    $('#btn_submit', dom).attr('src', G_TEMPLATES_STYLE + '/images/mobile/lead_button.png');
                } else if (!$('#reg_phone', dom).val() || !$('#verifycode', dom).val()) {
                    $('#btn_submit', dom).attr('src', G_TEMPLATES_STYLE + '/images/mobile/lead_unbutton.png');
                }
            }
        });
        $('#btn_submit', dom).off('click').on({
            click: function () {
                if (!$('#reg_phone', dom).val() || !$('#verifycode', dom).val()) {
                    return false;
                }
                var postData = it.getFormData('#step2');
                if (postData == false) {
                    return;
                }
                it.doAjax({
                    url: it.WEB_PATH + "/mobile/mobile/do_regist",
                    data: postData,
                    method: 'post',
                    success: function (res) {
                        if (res.code != 0) {
                            it.prompt(res.msg);
                            return;
                        }
                        it.doAjax({
                            url: it.WEB_PATH + "/mobile/mobile/do_send_gift",
                            success: function (res) {
                                if (res.code != 0) {
                                    it.prompt(res.msg);
                                    return;
                                }
                                $('.st_panel', dom).hide();
                                $('#step3', dom).fadeIn(500);
                            }
                        });
                    }
                });
            }
        });
        //step3
        $('#step3', dom).off('click').on({
            click: function () {
                dom.fadeOut(800, function () {
                    this.remove();
                });
            }
        });
    },
    flag_show: function () {
        var it = this;
        var flag_haswin = it.getCookie('flag_haswin');
        var flag_bind = it.getCookie('flag_bind');
        var flag_addr = it.getCookie('flag_addr');
        var flag_vir_addr = it.getCookie('flag_vir_addr');
        console.log('flag_bind:' + it.getCookie('flag_bind'), ' flag_addr:' + it.getCookie('flag_addr'), 'flag_vir_addr:' + it.getCookie('flag_vir_addr'));

        //判断中奖,无绑定手机,无绑定地址
        if (flag_haswin == 1 && flag_bind == 1 && (flag_addr == 1 || flag_vir_addr == 1)) {
            console.log('nobind_noaddr');
            //it.flag_st_nobind_noaddr(); //临时注销
            return;
        }

        //判断中奖,无绑定手机，已绑定地址
        if (flag_haswin == 1 && flag_bind == 1 && (flag_addr == undefined && flag_vir_addr == undefined)) {
            console.log('nobind_hasaddr');
            it.flag_st_nobind_hasaddr();
            return;
        }

        //判断中奖,已绑定手机，无绑定地址
        if (flag_haswin == 1 && flag_bind == undefined && (flag_addr == 1 || flag_vir_addr == 1)) {
            console.log('hasbind_noaddr');
            //it.flag_st_hasbind_noaddr(); //临时注销
            return;
        }

        //判断中奖,已绑定手机，已绑定地址
        if (flag_haswin == 1 && flag_bind == undefined && (flag_addr == undefined && flag_vir_addr == undefined)) {
            console.log('hasbind_hasaddr');
            it.flag_st_hasbind_hasaddr();
            return;
        }


        //
        if (it.getCookie('has_un_get_coupon') == 1) {
            console.log(it.getCookie('has_un_get_coupon'));
            it.flag_st_get_coupon();
            return;
        }


    },
    flag_st_get_coupon: function () {
        var it = this;
        /*$.get(WEB_PATH + '/mobile/mobile/get_coupon', {method: 1, page_size: 20}).then(function (res) {
         res = JSON.parse(res);
         if (res.code != 0) {
         it.prompt(res.msg);
         return;
         }


         if (res.data.other > 0) {
         it.flag_st_red_payment(res);
         }

         if (res.data.nohaswin > 0) {
         it.flag_st_red_packet_miss(res);
         }

         });*/

        it.doAjax({
            url: WEB_PATH + '/mobile/mobile/get_coupon',
            data: {method: 1},
            success: function (res) {
                if (res.data.other && res.data.other > 0) {
                    it.flag_st_red_payment(res);
                }

                if (res.data.nohaswin && res.data.nohaswin > 0) {
                    it.flag_st_red_packet_miss(res);
                }
            }

        });
    },
    /**
     *  判断中奖,无绑定手机,无绑定地址
     */
    flag_st_nobind_noaddr: function () {
        var it = this;
        var html = [
            '<div style="width:100%;height:100%;position:fixed;background:rgba(0,0,0,0.7);z-index:30;">',
            '<div style="position:absolute;left:50%;top:50%;width:300px;height:396px;margin-left:-150px;margin-top:-198px;">',
            '<img id="flag_bind" style="width:100%;cursor:pointer;" src="', G_TEMPLATES_STYLE, '/images/mobile/nobind_noaddr.png', '" >',
            '<input type="tel" maxlength="11" id="flag_phone" style="border:0;position:absolute;left:50px;top:180px;padding-left:10px;background:#fff;line-height:30px;height:30px;font-size:15px;" placeholder=" 输入手机号码" />',
            '<input type="tel" id="flag_verifycode" maxlength="8" name="phone" style="border:0;position:absolute;left:50px;top:215px;width:32%;padding-left:10px;background:#fff;height:30px;line-height:30px;font-size:15px;" placeholder=" 验证码" />',
            '<button type="tel" id="get_verifycode" style="position:absolute;left:166px;top:215px;background:#fe414c;color:#fff;border-radius:5px;font-size:14px;text-align:center;border:0;height:30px;line-height:30px;padding:0 5px;width:88px;outline:none;" >获取验证码</button>',
            '<div style="position:absolute;left:50px;top:125px;color:#e00a40;font-size:14px;">恭喜您被砸中啦!</div>',
            '<div style="position:absolute;left:50px;top:150px;color:#e00a40;font-size:14px;">请填写手机号以便我们与您联系</div>',
            '<div id="flag_notice" style="position:absolute;left:50px;top:251px;color:#e00a40;font-size:13px;line-height:normal;display:none;"></div>',
            '<div id="flag_next" style="position:absolute;left:95px;top:270px;padding:4px 5px;background:#fe414c;border-radius:5px;color:#fff;">继续完善地址<div>',
            '</div>',
            '</div>'
        ];
        var dom = $(html.join(""));
        $('#hasbind_hasaddr').remove();
        var url = decodeURIComponent(location.href);
        if (url.substring(url.lastIndexOf("/") + 1, url.length) != 'receiveaddr') {
            $('body').prepend(dom).css('overflow', 'hidden');
        }
        //it.bindEve(dom);
        $('#get_verifycode', dom).click(function () {
            var flag_phone = $('#flag_phone', dom).val();
            var This = $(this);
            if (flag_phone.match(/(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/) == null) {
                $('#flag_notice', dom).fadeIn().text('手机号码填写不正确, 请检查!');
                return;
            }
            it.doAjax({
                url: it.WEB_PATH + "/mobile/mobile/do_sendmsg/",
                data: {phone: flag_phone},
                method: 'post',
                success: function (res) {
                    if (res.code != 0) {
                        it.prompt(res.msg);
                        return;
                    }
                    it.showOk({
                        text: "短信验证码已发送成功, 请注意查收!",
                        callback: function () {
                            var loop = 60;
                            var value = This.html();
                            This.css('background', '#A5A5A5').attr('disabled', 'disabled');
                            var clock = setInterval(function () {
                                if (loop >= 0) {
                                    This.html((loop--) + ' S');
                                } else {
                                    This.css('background', '#fe414c').removeAttr('disabled').html(value);
                                    clearInterval(clock);
                                }
                            }, 1000);
                        }
                    });
                }
            });
        });

        //继续下一步
        $('#flag_next', dom).click(function () {
            var flag_phone = $('#flag_phone', dom).val();
            var flag_verifycode = $('#flag_verifycode', dom).val();
            if (flag_phone.match(/(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/) == null) {
                $('#flag_notice', dom).fadeIn().text('请先正确填写手机号码!');
                return;
            }
            if (flag_verifycode.match(/^[\d]+$/) == null) {
                $('#flag_notice', dom).fadeIn().text('请正确填写验证码!');
                return;
            }
            it.doAjax({
                url: it.WEB_PATH + "/mobile/mobile/do_regist/",
                method: 'post',
                data: {verifycode: flag_verifycode},
                success: function (res) {
                    if (res.code != 0) {
                        it.prompt(res.msg);
                        return;
                    }
                    var flag_vir_addr = it.getCookie('flag_vir_addr');
                    if (flag_vir_addr == 1) {
                        location.href = WEB_PATH + "/mobile/mobile/receiveaddr/#tab2";
                    } else {
                        location.href = WEB_PATH + "/mobile/mobile/receiveaddr/#tab1";
                    }

                }
            });
        });

    },
    /**
     *  判断中奖,无绑定手机，已绑定地址
     */
    flag_st_nobind_hasaddr: function () {
        var it = this;
        var html = [
            '<div style="width:100%;height:100%;position:fixed;background:rgba(0,0,0,0.7);z-index:30;">',
            '<div style="position:absolute;left:50%;top:50%;width:300px;height:396px;margin-left:-150px;margin-top:-198px;">',
            '<img id="flag_bind" style="width:100%;cursor:pointer;" src="', G_TEMPLATES_STYLE, '/images/mobile/nobind_noaddr.png', '" >',
            '<input type="tel" maxlength="11" id="flag_phone" style="border:0;position:absolute;left:50px;top:180px;padding-left:10px;background:#fff;line-height:30px;height:30px;font-size:15px;" placeholder=" 输入手机号码" />',
            '<input type="tel" id="flag_verifycode" maxlength="8" name="phone" style="border:0;position:absolute;left:50px;top:215px;width:32%;padding-left:10px;background:#fff;height:30px;line-height:30px;font-size:15px;" placeholder=" 验证码" />',
            '<button type="tel" id="get_verifycode" style="position:absolute;left:166px;top:215px;background:#fe414c;color:#fff;border-radius:5px;font-size:14px;text-align:center;border:0;height:30px;line-height:30px;padding:0 5px;width:88px;outline:none;" >获取验证码</button>',
            '<div style="position:absolute;left:50px;top:125px;color:#e00a40;font-size:14px;">恭喜您被砸中啦!</div>',
            '<div style="position:absolute;left:50px;top:150px;color:#e00a40;font-size:14px;">请填写手机号以便我们与您联系</div>',
            '<div id="flag_notice" style="position:absolute;left:50px;top:251px;color:#e00a40;font-size:13px;line-height:normal;display:none;"></div>',
            '<div id="flag_next" style="position:absolute;left:95px;top:270px;padding:4px 5px;background:#fe414c;border-radius:5px;color:#fff;">查看幸运记录<div>',
            '</div>',
            '</div>'
        ];
        var dom = $(html.join(""));
        $('#hasbind_hasaddr').remove();
        var url = decodeURIComponent(location.href);
        if (url.substring(url.lastIndexOf("/") + 1, url.length) != 'mywinrecord') {
            $('body').prepend(dom).css('overflow', 'hidden');
        }
        it.bindEve(dom);
    },
    bindEve: function (dom) {
        var it = this;
        //获取验证码
        $('#get_verifycode', dom).click(function () {
            var flag_phone = $('#flag_phone', dom).val();
            var This = $(this);
            if (flag_phone.match(/(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/) == null) {
                $('#flag_notice', dom).fadeIn().text('手机号码填写不正确, 请检查!');
                return;
            }
            it.doAjax({
                url: it.WEB_PATH + "/mobile/mobile/do_sendmsg/",
                data: {phone: flag_phone},
                method: 'post',
                success: function (res) {
                    if (res.code !== 0) {
                        it.prompt(res.msg);
                        return;
                    }
                    it.showOk({
                        text: "短信验证码已发送成功, 请注意查收!",
                        callback: function () {
                            var loop = 60;
                            var value = This.html();
                            This.css('background', '#A5A5A5').attr('disabled', 'disabled');
                            var clock = setInterval(function () {
                                if (loop >= 0) {
                                    This.html((loop--) + ' S');
                                } else {
                                    This.css('background', '#fe414c').removeAttr('disabled').html(value);
                                    clearInterval(clock);
                                }
                            }, 1000);
                        }
                    });
                }
            });
        });

        //继续下一步
        $('#flag_next', dom).click(function () {
            var flag_phone = $('#flag_phone', dom).val();
            var flag_verifycode = $('#flag_verifycode', dom).val();
            if (flag_phone.match(/(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/) == null) {
                $('#flag_notice', dom).fadeIn().text('请先正确填写手机号码!');
                return;
            }
            if (flag_verifycode.match(/^[\d]+$/) == null) {
                $('#flag_notice', dom).fadeIn().text('请正确填写验证码!');
                return;
            }
            it.doAjax({
                url: it.WEB_PATH + "/mobile/mobile/do_regist/",
                method: 'post',
                data: {verifycode: flag_verifycode},
                success: function (res) {
                    if (res.code != 0) {
                        it.prompt(res.msg);
                        return;
                    }
                    location.href = WEB_PATH + "/mobile/mobile/mywinrecord";
                }
            });

        });
    },
    /**
     *  判断中奖,已绑定手机，无绑定地址
     */
    flag_st_hasbind_noaddr: function () {
        var it = this;
        var flag_vir_addr = it.getCookie('flag_vir_addr');
        if (flag_vir_addr == 1) {
            var href_path = WEB_PATH + "/mobile/mobile/receiveaddr/#tab2";
        } else {
            var href_path = WEB_PATH + "/mobile/mobile/receiveaddr/#tab1";
        }
        var html = [
            '<div id="hasbind_noaddr" style="width:100%;height:100%;position:fixed;background:rgba(0,0,0,0.7);z-index:30;">',
            '<div style="position:absolute;left:50%;top:50%;width:300px;height:454px;margin-left:-150px;margin-top:-227px;">',
            '<a href="', href_path, '"><img id="flag_bind" style="width:100%;cursor:pointer;" src="', G_TEMPLATES_STYLE, '/images/mobile/hasbind_noaddr.png', '" ></a>',
            '</div>',
            '</div>'
        ];
        var url = decodeURIComponent(location.href);
        $('#hasbind_noaddr').remove();
        if (url.substring(url.lastIndexOf("/") + 1, url.length) != 'receiveaddr') {
            var dom = $(html.join(""));
            $('body').prepend(dom).css('overflow', 'hidden');
        }
    },
    /**
     *  判断中奖,已绑定手机，已绑定地址
     */
    flag_st_hasbind_hasaddr: function () {
        var it = this;
        var html = [
            '<div id="hasbind_hasaddr" style="width:100%;height:100%;position:fixed;background:rgba(0,0,0,0.7);z-index:30;">',
            '<div style="position:absolute;left:50%;top:50%;width:300px;height:454px;margin-left:-150px;margin-top:-227px;">',
            '<img id="flag_bind" style="width:100%;cursor:pointer;" src="', G_TEMPLATES_STYLE, '/images/mobile/hasbind_hasaddr.png', '" >',
            '</div>',
            '</div>'
        ];
        var dom = $(html.join(""));
        $('#hasbind_hasaddr').remove();
        $('body').prepend(dom).css('overflow', 'hidden');
        //it.delCookie('flag_haswin');
        //it.setCookie('flag_haswin', 'has_sign');
        $('#flag_bind', dom).off('click').on({
            click: function () {
                it.doAjax({
                    url: it.WEB_PATH + '/mobile/mobile/do_update_haswin',
                    method: 'post',
                    success: function (res) {
                        if (res.code !== 0) {
                            it.alert(res.msg);
                            return;
                        }
                        dom.remove();
                        location.href = WEB_PATH + '/mobile/mobile/usercenter';
                    },
                });
            }
        });
    },
    /*
     * 红包弹窗
     * */
    flag_st_red_payment: function (res) {
        //红包弹窗//模版来自footer.html
        var $hotPacket = $($('#hot-packet-tpl').html());
        console.log(res);
        $hotPacket.find('.hp-num').html(res.data.other);//设置红包数
        var liTpl = $hotPacket.find('ul li').get(0);
        $hotPacket.find('ul').empty();

        //console.log(liTpl);
        var ids = [];
        var obj = res.data.coupon_info.rows.other;
        if (obj) {
            obj.map(function (item) {
                //console.log(66, item);
                var $li = $(liTpl).clone(true);
                $li.find('.hot-packet-sum').text(parseInt(item.amount));
                $li.find('.limit-type').text(item.limit_type);
                $li.find('.limit-num').text('满' + item.limit_amount + '元可用');
                //$li.find('.tween-time').text(item.start_time.substr(0, 10) + '至' + item.end_time.substr(0, 10));
                $li.appendTo($hotPacket.find('ul'));
                ids.push(item.id);
            });
        }
        $('body').prepend($hotPacket);
        $hotPacket.find('.lead_delete').on('click', function () {
            //console.log(ids);
            $.post(WEB_PATH + '/mobile/mobile/do_get_coupon', {ids: ids});
            $hotPacket.remove();
            //条转到红包列表
            window.location.href = '/mobile/mobile/my_coupon';
        });
        $hotPacket.find('.gotit').on('click', function () {
            $.post(WEB_PATH + '/mobile/mobile/do_get_coupon', {ids: ids});
            $hotPacket.remove();
            //条转到红包列表
            window.location.href = '/mobile/mobile/my_coupon';
        });
    },
    /*
     * 红包，未中奖弹窗
     * */
    flag_st_red_packet_miss: function (res) {

        var $hotPacketMiss = $($('#hot-packet-miss-tpl').html());
        //console.log($hotPacketMiss);
        $('body').prepend($hotPacketMiss);
        var ids = [];
        res.data.coupon_info.rows.nohaswin.map(function (item) {
            ids.push(item.id);
        });

        $hotPacketMiss.find('.hot-packet-miss-btn').on('click', function (e) {
            $hotPacketMiss.remove();
            $.post(WEB_PATH + '/mobile/mobile/do_get_coupon', {ids: ids});
            window.location.href = '/mobile/mobile/my_coupon';
        })
    }
});


