defineClass('init', basejs, {
    init: function () {
        var it = this;
//      it.loadSlid();
//      it.onNotice();
        //it.onAtresult();
        //it.onCate();
//      it.onHotShop();
    },
    /**
     * 幻灯片
     * @returns {undefined}
     */
    loadSlid: function () {
        var it = this;
        it.list({
            url: "/api/api_ads/get_ads_list",
            method: 'get',
            postData: {
                rows: 10,
                order: "id desc"
            },
            loadList: function (res) {
                var res = res.data;
                if (res.rows.length > 0) {
                    var html = [
                        '<ul class = "swiper-wrapper">',
                    ];
                    for (var i in res.rows) {
                        var item = res.rows[i];
                        var img = item['img'] || "";
                        if (img.indexOf("://") == -1) {
                            //如果是相对路径，加上域名及目录
                            img = G_UPLOAD_PATH + "ad_ads/" + img;
                        }
                        html.push(
                                '<li class="swiper-slide" style="background-color:', item['alt'], ';">',
                                '<a href="', item['link'], '"><img src="', img, '" alt="" style="width:100%;"/></a>',
                                '</li>'
                                );
                    }
                    html.push('</ul>');
                    html.push('<div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets"></div>');
                    $("#sliderBox").append(html.join(""));
                    new Swiper('#sliderBox', {
                        pagination: '#sliderBox .swiper-pagination',
                        paginationClickable: true,
                        autoplay: 2500
                    });
                }
            }
        });
    },
    /**
     * 最新揭晓
     * @returns {undefined}
     */
    onNotice: function () {
        var it = this;
        it.doAjax({
            url: "/api/api_ads/get_notice_list",
            success: function (res) {
                var res = res.data;
                var html = [];
                var length = res.length, index = 1;
                for (var i in res) {
                    var item = res[i];
                    html.push(
                            '<a>',
                            '<p class="noticeItem span2 ellipsis" style="font-size:11px;height:30px;line-height:30px;">',
                            '通知： &nbsp;<span style="color:#1194f7;">', item,
                            '</p></a>'
                            );
                }
                $("#notice_content").html(html.join(""));
                $("#notice_content .noticeItem").slideUp();
                $("#notice_content .noticeItem:eq(0)").slideDown('fast');
                var loop = function () {
                    setTimeout(function () {
                        if (index > length - 1) {
                            index = 0;
                        }
                        $("#notice_content .noticeItem").slideUp();
                        $('#notice_content .noticeItem:eq( ' + index + ')').slideDown(1000);
                        index++;
                        loop();
                    }, 3500);
                };
                loop();
            }
        });
    },
    /**
     * 即将揭晓
     * @returns {undefined}
     */
    onAtresult: function () {
        var it = this;
        it.doAjax({
            url: it.WEB_PATH + "/mobile/mobile/getatresult/",
            success: function (res) {
                if (res && res.data && res.data.length > 0 && res.code === 0) {
                    var html = [
                        '<div class="swiper-container swiper-container-horizontal" style="padding:5px 0;border-top:solid 12px #E9E9E9;">',
                        '<ul class="swiper-wrapper">'
                    ];
                    for (var i in res.data) {
                        var item = res.data[i];
                        var img = item['thumb'] || "";
                        if (img.indexOf("://") == -1) {
                            //如果是相对路径，加上域名及目录
                            img = it.WEB_PATH + "/statics/uploads/" + img;
                        }
                        html.push(
                                '<li class="swiper-slide" style="text-align:center;padding-top:5px;border-right:solid 1px #EAEAEA;float:left;display:block;" onclick="javascript:location.href = \'',
                                it.WEB_PATH, '/mobile/mobile/shopinfo/?id=', item['id'], '\';">',
                                '<img src="', img, '" style="width:65px;height:65px;display:block;margin:0 auto;"/>',
                                '<div class="q_end_time" endtime="', item['xsjx_time'], '"></div>',
                                '</li>'
                                );
                    }
                    if (res.data.length === 12) {
                        html.push(
                                '<li class="swiper-slide" style="text-align:center;padding-top:5px;border-right:solid 1px #C7CBCC;display:block;" onclick="javascript:location.href = \'',
                                it.WEB_PATH, '/mobile/mobile/atresult/\';">',
                                '<div style="width:40px;height:40px;color:#FFF;background:#FF8400;border-radius:50%;vertical-align:middle;text-align:center;font-size:15px;margin:25px auto;padding:10px;line-height:20px;">查看<br/>更多</div>',
                                '</li>'
                                );
                    }
                    html.push('</ul>', '</div>');
                    $("#atresult").html(html.join(""));
                    new Swiper('#atresult .swiper-container', {
                        autoplay: 2500,
                        slidesPerView: 4,
                        grabCursor: true
                    });
                    it.initTimeout();
                }
            }
        });
    },
    initTimeout: function () {
        var it = this;
        it.onTimeout("#atresult .q_end_time", 85, "mm:ss:SS", function (index, dom) {
            $(dom).html("已揭晓");
        });
    },
    onCate: function () {
        var it = this;
        var hotShopParam = {
            cate: "all",
            order: "canyurenshu/zongrenshu*100 desc,`order` desc"
        };
        if (localStorage.getItem("hotShopParam")) {
            hotShopParam = it.toJson(localStorage.getItem("hotShopParam"));
        } else {
            localStorage.setItem("hotShopParam", it.toString(hotShopParam));
        }
        $(".shop_cate .row a").removeClass("current");
        var current_cate = $(".shop_cate .row a[cate='" + hotShopParam.cate + "']").addClass("current").text();
        $("#current_cate span").text(current_cate);
        $("#current_cate").off("click").on({
            click: function () {
                if ($(".shop_cate").is(":hidden")) {
                    $(".shop_cate").slideDown();
                    $("#current_cate img").attr("src", it.G_TEMPLATES_STYLE + "/images/mobile/fen_down.png");
                } else {
                    $(".shop_cate").slideUp();
                    $("#current_cate img").attr("src", it.G_TEMPLATES_STYLE + "/images/mobile/fen_up.png");
                }
            }
        });
        $(".shop_cate .row a").off("click").on({
            click: function () {
                $(".shop_cate .row a").removeClass("current");
                var current_cate = $(this).addClass("current").text();
                $("#current_cate span").text(current_cate);
                $(".shop_cate").slideUp();
                $("#current_cate img").attr("src", it.G_TEMPLATES_STYLE + "/images/mobile/fen_up.png");
                hotShopParam.cate = $(this).attr("cate");
                localStorage.setItem("hotShopParam", it.toString(hotShopParam));
                it.onHotShop();
            }
        });
        $("#rmsp>ul>li").removeClass("current");
        $("#rmsp>ul>li[order='" + hotShopParam.order + "']").addClass("current");
        $("#rmsp>ul>li[id!='current_cate']").off("click").on({
            click: function () {
                $("#rmsp>ul>li").removeClass("current");
                $(this).addClass("current");
                $(".shop_cate").slideUp();
                hotShopParam.order = $(this).attr("order");
                localStorage.setItem("hotShopParam", it.toString(hotShopParam));

                /**
                 * 再次点击，相反排序 Allen
                 */
//                if (hotShopParam.order.indexOf('asc') > 1) {
//                    $(this).attr('order', hotShopParam.order.replace(/asc/, 'desc'));
//                } else {
//                    $(this).attr('order', hotShopParam.order.replace(/desc/, 'asc'));
//                }
                it.onHotShop();
            }
        });
        var rheight = $("#rmsp").offset().top;
        $(document).scroll(function () {
            if (!$("#rmsp").is(".rmspfixed")) {
                rheight = $("#rmsp").offset().top;
            }
            if ($(document).scrollTop() >= rheight) {
                $("#rmsp").addClass("rmspfixed");
            } else {
                $("#rmsp").removeClass("rmspfixed");
            }
        });
    },
    /**
     * 热门商品
     * @returns {undefined}
     */
    onHotShop: function () {
        var it = this;
        var hotShopParam = it.toJson(localStorage.getItem("hotShopParam"));
        var order = hotShopParam.order;
        $("#list_content").html("");
        it.list({
            url: "/api/api_ads/get_shop_list",
            postData: {
                order: order,
                page_size: 30,
                filter: ""
            },
            loadList: function (res) {
                var html = [];
                var res = res.data.result;
                for (var i in res) {
                    var item = res[i];
                    var img = item['Pic'] || "";
                    html.push(
                            '<div class="weui_grid shop_item" shopid="', item['id'], '" maxnum="', item['maxnum'], '" restnum="', item['shenyurenshu'], '" prenum="', item['prenum'], '" style="width:50%;padding:0;">',
                            '<a href="/home/goodDetail" style="display:block;">',
                            '<div style="text-align:center;height: 180px;">',
                            '<img src="', img, '" style="width:100%;height:100%;" class="shop_item_img">',
                            '</div>',
                            '</a>',
                            '<div style="" class="good-bottom">',
                            	'<p class="span1 ellipsis" style="font-size:14px;padding-top:0px;line-height:21px;height: 40px;-webkit-box-orient: vertical;-webkit-line-clamp: 2;">', item['D_title'], '</p>',
	                            '<div style="padding:1px 0;overflow:hidden;position:relative;height:20px;">',
	                            '<span style="font-size:12px;color:#383838;position:absolute;left:0;"><span class="old_price">￥49</span><span class="curent_price">￥', item['Org_Price'], '</span></span>',
	                            '<span style="font-size:12px;color:#ccc;position:absolute;right:0;">已购：', item['Sales_num'], '</span>',
	                            '</div>',
	                            '<div style="padding:1px 0;overflow:hidden;position:relative;height:20px;margin-bottom: 10px;">',
	                            '<span style="font-size:12px;color:#fff;background:#fd312a;display:inline-block;padding:0 5px;position:absolute;right:0;">', item['Price'], '元券</span>',
	                            '<span style="font-size:12px;color:#383838;position:absolute;left:0; "><i class="fa fa-money" aria-hidden="true"></i><font style="color:#f00;font-weight:bold;">￥', item['Quan_price'], '</font></span>',
	                            '</div>',
                            '</div>',
                            '</div>'
                            );
                }
                $("#list_content").append(html.join(""));
                it.onAddShop();
            }
        });
    },
    onAddShop: function () {
        var it = this;
        $(".add_shop").off("click").on({
            click: function (event) {
                var dom = $(this).parents(".shop_item");
                var maxnum = Number($(dom).attr("maxnum"));
                var shopid = Number($(dom).attr("shopid"));
                var restnum = Number($(dom).attr("restnum"));
                var prenum = Number($(dom).attr("prenum"));
                var isNewShop = true;
                var shopCarList = localStorage.getItem("shopCarList") || "[]";
                shopCarList = it.toJson(shopCarList);
                var shop = {
                    shopid: shopid,
                    shopnum: it.min([prenum, maxnum, restnum])
                };
                for (var i in shopCarList) {
                    var item = shopCarList[i];
                    if (item.shopid === shopid) {
                        isNewShop = false;
                        shop.shopnum = it.min([item.shopnum + prenum, maxnum, restnum]);
                        shopCarList[i] = shop;
                        break;
                    }
                }
                if (isNewShop) {
                    shopCarList.push(shop);
                }
                var shop_item_num = shopCarList.length;
                shopCarList = it.toString(shopCarList);
                localStorage.setItem("shopCarList", shopCarList);
                var offset = $(".shop_car_num").offset();
                var flyer = $('<img src="' + it.G_TEMPLATES_STYLE + '/images/mobile/add.png" style="width:23px;height:23px;"/>');
                flyer.fly({
                    start: {
                        left: event.clientX - 10,
                        top: event.clientY - 28,
                    },
                    end: {
                        left: offset.left,
                        top: $(window).height() - 20,
                        width: 0,
                        height: 0,
                        style: "width:0px;height:0px;"
                    },
                    onEnd: function () {
                        $(".shop_car_num").removeClass("offsethide").html(shop_item_num).show();
                        $(flyer).remove();
                    }
                });

            }
        });
    }
});