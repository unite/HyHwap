var htx = {
	/**
	 * 获取url的参数值
	 * @param {Object} name
	 */
	getUrlParam: function(name) {
		var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
		var r = window.location.search.substr(1).match(reg); //匹配目标参数
		if(r != null)
			return unescape(r[2]);
		return null; //返回参数值
	},

	/**
	 * 
	 * @param {Object} el  节点
	 * @param {Object} count   展示的按钮的总数
	 * @param {Object} sumNum  数据总数
	 * @param {Function} callback  回调
	 * @param {Function} page_no  当前页
	 */

	setPaginator: function(el, count, sumNum, pagesize, page_no, callback) {
		if($(el)) {
			$.jqPaginator(el, {
				totalPages: Math.ceil(sumNum / pagesize),
				visiblePages: count || 5,
				currentPage: page_no,
				wrapper: '<ul class="pagination"></ul>',
				first: '<li class="first"><a href="javascript:void(0);">首页</a></li>',
				prev: '<li class="prev"><a href="javascript:void(0);">上一页</a></li>',
				next: '<li class="next"><a href="javascript:void(0);">下一页</a></li>',
				last: '<li class="last"><a href="javascript:void(0);">尾页</a></li>',
				page: '<li class="page"><a href="javascript:void(0);"></a></li>',
				onPageChange: function(num) {
					console.log(num)
					if($.isFunction(callback)){
						callback(num);
						
					}
				}
			});
		}

		
	}
}