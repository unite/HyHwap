<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . '/controllers/Base.php';

class Home extends Base {

//  public function index()
//  {
//      $this->data['key'] = '首页';
//      $this->twig->render('front/index.html', $this->data);
//  }
//	
	public function forward()
    {
        $this->data['key'] = '我的钱包';
        $this->twig->render('front/longcai/forward.html', $this->data);
    }
    
    public function forwardTwo(){
		$this->data['key'] = '我要提现';
        $this->twig->render('front/longcai/forwardTwo.html', $this->data);
	}
	
	 public function forwardThree(){
		$this->data['key'] = '我要提现第二步';
        $this->twig->render('front/longcai/forwardThree.html', $this->data);
	}
	
	public function forwardone()
    {
        $this->data['key'] = '提现';
        $this->twig->render('front/longcai/forwardone.html', $this->data);
    }
	
	public function forwardlist()
    {
        $this->data['key'] = '提现记录';
        $this->twig->render('front/longcai/forwardlist.html', $this->data);
    }
    
	
	public function addCard()
    {
        $this->data['key'] = '添加银行卡';
        $this->twig->render('front/longcai/addCard.html', $this->data);
    }
    public function addphone()
    {
        $this->data['key'] = '添加银行卡';
        $this->twig->render('front/longcai/addphone.html', $this->data);
    }
    
	public function forgetpwd()
    {
        $this->data['key'] = '修改密码';
        $this->twig->render('front/longcai/forget.html', $this->data);
    }
	public function givedetails()
    {
        $this->data['key'] = '提现详情';
        $this->twig->render('front/longcai/givedetails.html', $this->data);
    }
	public function accountBankCard()
    {
        $this->data['key'] = '到账银行卡';
        $this->twig->render('front/longcai/accountBankCard.html', $this->data);
    }
	public function forgetpwdOne()
    {
        $this->data['key'] = '修改密码';
        $this->twig->render('front/longcai/forgetOne.html', $this->data);
    }
	
	public function advInfo()
    {
        $this->data['key'] = '广告详情';
        $this->twig->render('front/longcai/advInfo1.html', $this->data);
    }
	
	public function advInfo_mobile ()
    {
        $this->data['key'] = '广告分享详情';
        $this->twig->render('front/longcai/advInfo.html', $this->data);
    }
	
	public function otheradvInfo2()
    {
        $this->data['key'] = '广告详情';
        $this->twig->render('front/longcai/advInfo2.html', $this->data);
    }
	
	public function ad ()
    {
        $this->data['key'] = '广告分享详情';
        $this->twig->render('front/longcai/ad1.html', $this->data);
    }
	
	public function wallte(){
		$this->data['key'] = '我的钱包';
        $this->twig->render('front/longcai/wallet.html', $this->data);
	}
    
	public function login(){
		$this->data['key'] = '登录';
        $this->twig->render('front/longcai/login.html', $this->data);
	}
	
	public function login1(){
		$this->data['key'] = '账号登录';
        $this->twig->render('front/longcai/login1.html', $this->data);
	}
	
	public function login2(){
		$this->data['key'] = '账号登录';
        $this->twig->render('front/longcai/login2.html', $this->data);
	}
	
	public function test(){
		$this->data['key'] = '账号登录';
        $this->twig->render('front/longcai/test.html', $this->data);
	}
	public function test1(){
		$this->data['key'] = '账号登录';
        $this->twig->render('front/longcai/test1.html', $this->data);
	}
	
	public function getRule(){
		$this->data['key'] = '查看规则';
        $this->twig->render('front/longcai/rule.html', $this->data);
	}
	
	public function map(){
		$this->data['key'] = '地图';
        $this->twig->render('front/longcai/map.html', $this->data);
	}
	
	public function suspension(){
		$this->data['key'] = '悬浮';
        $this->twig->render('front/longcai/suspension.html', $this->data);
	}
	
	public function suspension_mobile(){
		$this->data['key'] = '悬浮1';
        $this->twig->render('front/longcai/suspension1.html', $this->data);
	}
	
	
	public function shadvInfo(){
		$this->data['key'] = '审核详情';
        $this->twig->render('front/longcai/shadvInfo.html', $this->data);
	}
	
	
	
	public function wx(){
		$this->data['key'] = '微信测试';
        $this->twig->render('front/longcai/wx.html', $this->data);
	}
	
	public function hearder(){
		$this->data['key'] = 'hearder';
        $this->twig->render('front/longcai/hearder.html', $this->data);
	}

}
