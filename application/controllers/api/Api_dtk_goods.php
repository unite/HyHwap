<?php

/*
 * 大淘客 商品相关 
 */

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . '/controllers/api/Apibase.php';

class Api_dtk_goods extends Apibase {

    function __construct()
    {
        parent::__construct();
        //$this->load->model('pm/pm_goods');
        //$this->model = $this->pm_goods;
    }

    public function get_web_goods_get()
    {
        //验证请求数据 
        $request_data = $this->check_param([
            'order' => ['排序', 'trim'],
            'keyword' => ['商品名称', 'max_length[100]'],
            'page_no' => ['当前页', 'integer'],
            'page_size' => ['每页显示数', 'integer'],
        ]);

        /*
         * 验证成功后的逻辑
         * 1.校验参数有效性(已前置处理)
         * 2.查询商品列表数据
         */
        $this->load->library('dataoke');
        $res = $this->dataoke->get_web_goods($request_data);
        //print_r($res);
        //$res = array_merge($res, ['page_no' => $request_data['page_no'], 'page_size' => $request_data['page_size']]);
        $this->returnData($res);
    }

    /**
     * TOP100人气榜API接口
     */
    public function get_top100_goods_get()
    {
        //验证请求数据 
        $request_data = $this->check_param([
            'order' => ['排序', 'trim'],
            'keyword' => ['商品名称', 'max_length[100]'],
            'page_no' => ['当前页', 'integer'],
            'page_size' => ['每页显示数', 'integer'],
        ]);

        /*
         * 验证成功后的逻辑
         * 1.校验参数有效性(已前置处理)
         * 2.查询商品列表数据
         */
        $this->load->library('dataoke');
        $res = $this->dataoke->get_top100_goods($request_data);
        //print_r($res);
        //$res = array_merge($res, ['page_no' => $request_data['page_no'], 'page_size' => $request_data['page_size']]);
        $this->returnData($res);
    }

    /**
     * 全站领券商品API接口
     */
    public function get_hasgift_goods_get()
    {
        //验证请求数据 
        $request_data = $this->check_param([
            'order' => ['排序', 'trim'],
            'keyword' => ['商品名称', 'max_length[100]'],
            'page_no' => ['当前页', 'integer'],
            'page_size' => ['每页显示数', 'integer'],
        ]);

        /*
         * 验证成功后的逻辑
         * 1.校验参数有效性(已前置处理)
         * 2.查询商品列表数据
         */
        $this->load->library('dataoke');
        $res = $this->dataoke->get_hasgift_goods($request_data);
        print_r($res);
        //$res = array_merge($res, ['page_no' => $request_data['page_no'], 'page_size' => $request_data['page_size']]);
        $this->returnData($res);
    }

    /**
     * 实时跑量榜API接口
     */
    public function get_goods_trunk_get()
    {
        //验证请求数据 
        $request_data = $this->check_param([
            'order' => ['排序', 'trim'],
            'keyword' => ['商品名称', 'max_length[100]'],
            'page_no' => ['当前页', 'integer'],
            'page_size' => ['每页显示数', 'integer'],
        ]);

        /*
         * 验证成功后的逻辑
         * 1.校验参数有效性(已前置处理)
         * 2.查询商品列表数据
         */
        $this->load->library('dataoke');
        $res = $this->dataoke->get_goods_trunk($request_data);
        //print_r($res);
        //$res = array_merge($res, ['page_no' => $request_data['page_no'], 'page_size' => $request_data['page_size']]);
        $this->returnData($res);
    }

    /**
     * 单品详情API接口
     */
    public function get_goods_info_get()
    {
        //验证请求数据 
        $request_data = $this->check_param([
            'goods_id' => ['商品名称', 'required', 'integer'],
        ]);

        /*
         * 验证成功后的逻辑
         * 1.校验参数有效性(已前置处理)
         * 2.查询商品列表数据
         */
        $this->load->library('dataoke');
        $res = $this->dataoke->get_goods_info($request_data);
        //print_r($res);
        //$res = array_merge($res, ['page_no' => $request_data['page_no'], 'page_size' => $request_data['page_size']]);
        $this->returnData($res);
    }

}
