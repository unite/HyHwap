<?php

/*
 * 广告相关
 */

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . '/controllers/api/Apibase.php';

class Api_ads extends Apibase {

    public $model;

    function __construct()
    {
        parent::__construct();
        $this->load->model('am/am_ads');
        $this->model = $this->am_ads;
    }

    /**
     * 获取广告
     */
    public function get_ads_list_post()
    {
        //验证请求数据 
        $request_data = $this->check_param([
            'type' => ['广告类型', 'integer'], //type 1:启动页,2:首页banner,3:底部广告
                ], [], 'post');
        /*
         * 验证成功后的逻辑
         * 1.校验参数有效性(已前置处理)
         * 2.查询广告列表数据
         */
        $sql = $this->model->getListSql('*', [
                //'type' => $request_data['type'],
                //'is_show' => 1,
                //'UNIX_TIMESTAMP(begin_at)<' => $this->_start_rtime, //暂时不支持上下架功能
                //'UNIX_TIMESTAMP(end_at)>' => $this->_start_rtime//暂时不支持上下架功能
        ]);
        $grid = $this->grid($sql, 'id desc');
        $this->returnData($grid);
    }

    /**
     * 获取消息通知列表 （前期测试）
     */
    public function get_notice_list_get()
    {
        $data = [
            '红猪快购，千元大奖等你来赚哦...',
            '2018年春节，新年扫年货，抢到就是赚到...',
            '通知文字通知文字，通知文字，通知文字，通知文字 ...'
        ];
        $this->returnData($data);
    }

    /**
     * 获取商品
     */
    public function get_shop_list_post()
    {
        //验证请求数据 
        $request_data = $this->check_param([
            'page_size' => ['类型', 'integer'],
            'page' => ['类型', 'integer'],
                ], [], 'post');
        $this->load->library('dataoke');
        //$res = $this->dataoke->get_web_goods($request_data['page_size']);
        $res = $this->dataoke->get_top100_goods();
        //$this->load->library('haodanku');
        //$res = $this->haodanku->get_goods_list($request_data['page_size']);
        //get_goods_list
        $this->returnData($res);
    }

}
