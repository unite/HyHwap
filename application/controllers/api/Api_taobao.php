<?php

/*
 * 商品相关 
 */

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . '/controllers/api/Apibase.php';

class Api_taobao extends Apibase {

    function __construct()
    {
        parent::__construct();
    }

    /**
     * 获取商品列表
     * order,downnum:热度，下载次数，weight:推荐权重，new:最新
     */
    public function get_tb_goods_get()
    {
        //验证请求数据 
        $request_data = $this->check_param([
            'order' => ['排序', 'trim'],
            'keyword' => ['商品名称', 'max_length[100]'],
            'page_no' => ['当前页', 'integer'],
            'page_size' => ['每页显示数', 'integer'],
        ]);
        $this->load->library('taobaoke');
        $res = $this->taobaoke->tbk_item_get($request_data);
        /*
         * 验证成功后的逻辑
         * 1.校验参数有效性(已前置处理)
         * 2.查询商品列表数据
         */
        $res = array_merge($res, ['page_no' => $request_data['page_no'], 'page_size' => $request_data['page_size']]);
        $this->returnData($res);
    }

    /**
     * 好券清单API【导购】
     */
    public function get_tb_coupon_get()
    {
        //验证请求数据 
        $request_data = $this->check_param([
            'keyword' => ['查询词', 'max_length[100]'],
            'page_no' => ['当前页', 'integer'],
            'page_size' => ['每页显示数', 'integer'],
        ]);

        /*
         * 验证成功后的逻辑
         * 1.校验参数有效性(已前置处理)
         * 2.查询商品列表数据
         */
        $cat = '16,18';
        $this->load->library('taobaoke');
        $res = $this->taobaoke->tbk_dg_item_coupon($request_data['keyword'], $cat, $request_data['page_no'], $request_data['page_size']);
        $res = array_merge($res, ['page_no' => $request_data['page_no'], 'page_size' => $request_data['page_size']]);
        $this->returnData($res);
    }

    /**
     * 获取淘宝类目列表
     */
    public function get_goods_catid_get()
    {
        /*
         * 验证成功后的逻辑
         * 1.校验参数有效性(已前置处理)
         * 2.查询商品列表数据
         */
        $res = [];
        $this->returnData($res);
    }

    /**
     * 阿里妈妈推广券信息查询
     */
    public function get_tb_coupon_search_get()
    {
        //验证请求数据 
        $request_data = $this->check_param([
            'key' => ['带券ID与商品ID的加密串 ', 'max_length[100]'],
        ]);

        /*
         * 验证成功后的逻辑
         * 1.校验参数有效性(已前置处理)
         * 2.查询商品列表数据
         */
        $this->load->library('taobaoke');
        $res = $this->taobaoke->tbk_coupon_search($request_data['key']);
        $this->returnData($res);
    }

    /**
     * 物料传播方式获取,生成推广链接
     */
    public function get_tb_goods_link_get()
    {
        //验证请求数据 
        $request_data = $this->check_param([
            'url_list' => ['地址列表 ', 'max_length[10000]'],
        ]);
        /*
         * 验证成功后的逻辑
         * 1.校验参数有效性(已前置处理)
         * 2.查询商品列表数据
         */
        $this->load->library('taobaoke');
        $res = $this->taobaoke->tbk_spread($request_data['url_list']);
        $this->returnData($res);
    }

    /**
     * 获取商品详情
     */
    public function get_tb_goods_info_get()
    {
        //验证请求数据 
        $request_data = $this->check_param([
            'order' => ['排序', 'trim'],
        ]);

        $this->load->library('taobaoke');
        $res = $this->taobaoke->tbk_item_info_get('1731008838,530742099355');
        /*
         * 验证成功后的逻辑
         * 1.校验参数有效性(已前置处理)
         * 2.查询商品列表数据
         */
        $this->returnData($res);
    }

    /**
     * 淘宝客商品链接转换
     */
    public function test_tbk_tpwd_create_get()
    {
        $res = $this->tbk_tpwd_create($user_id = '1', $text = '云客派淘口令', $url = 'https://uland.taobao.com/', $logo = 'https://uland.taobao.com/', $ext = '{}');
        $this->returnData($res);
    }

    /**
     * 淘宝客商品链接转换
     */
    public function test_tbk_item_convert_get()
    {
        $res = $this->tbk_item_convert($num_iids = '1731008838,530742099355', $adzone_id = '123');
        $this->returnData($res);
    }

    /* ------------------------------------------------------- 以下是私有方法, 内部调用 ------------------------------------------------------- */

    /**
     * 淘宝客商品链接转换
     */
    private function tbk_item_convert($num_iids, $adzone_id)
    {
        $this->load->library('taobaoke');
        $res = $this->taobaoke->tbk_item_convert($num_iids, $adzone_id);
        return $res;
    }

    /**
     * 淘宝客商品链接转换
     */
    private function tbk_tpwd_create($user_id, $text, $url, $logo, $ext)
    {
        $this->load->library('taobaoke');
        $res = $this->taobaoke->tbk_tpwd_create($user_id, $text, $url, $logo);
        return $res;
    }

}
